# Customer API
API para gerenciamento de clientes.

### Base de dados
O banco de dados que está sendo utilizado é o Postgres. Ele pode ser executado isoladamente de 2 maneiras:

a partir da imagem [devbrsam/postgresdb:1.0.0](https://hub.docker.com/r/devbrsam/postgresdb/tags):

```bash
docker run -d --name postgresdb -p 5432:5432 devbrsam/postgresdb:1.0.0
```
executando o [docker-compose](docker-compose.yml):

```bash
docker-compose up --scale customer=0 
```

### API
Para executar a aplicação, importe o projeto em sua IDE ou execute o comando maven abaixo:

```bash
mvn spring-boot:run
```

É possível executar a aplicação e o banco de dados em conjunto com base no [docker-compose](docker-compose.yml) presente na raiz do projeto.

```bash
docker-compose up 
```
É possível também executar este docker-compose usando diretamente o Dockerfile criado para gerar a imagem do banco de dados. Para isso, é necessário alterar o [docker-compose.yml](docker-compose.yml) apontando para o [Dockerfile](.postgres/Dockerfile) do banco:

```bash
services:
  postgres-db:
    build: .postgres 
```

Depois de alterar o [docker-compose](docker-compose.yml), faça o build:
```bash
docker-compose build
```
### Documentações
##### [Swagger](http://localhost:8000/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config) (local)

##### [Postman](https://documenter.getpostman.com/view/5187974/TVep9TnF) (online)  
Ou importe a coleção: 

[![Run in Postman](https://run.pstmn.io/button.svg)](https://god.postman.co/run-collection/8fcb9f16863756b585b8)