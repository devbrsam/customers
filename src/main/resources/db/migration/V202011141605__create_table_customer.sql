CREATE TABLE Customer (

    id uuid DEFAULT uuid_generate_v4(),
    name VARCHAR(100) NOT NULL,
    cpf VARCHAR(11) NOT NULL,
    birth_date DATE,

    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP,

    PRIMARY KEY (id)
);

CREATE INDEX IX_CUSTOMER_CPF ON Customer (cpf);