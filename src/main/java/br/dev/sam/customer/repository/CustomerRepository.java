package br.dev.sam.customer.repository;

import br.dev.sam.customer.dto.CustomerDto;
import br.dev.sam.customer.filter.CustomerFilter;
import br.dev.sam.customer.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, UUID> {

    List<Customer> findCustomerByNameIgnoreCaseContainingOrCpfContaining(String name, String cpf);
    List<Customer> findCustomerByNameIgnoreCaseContaining(String name);
    List<Customer> findCustomerByCpfContaining(String cpf);

    @Query("  SELECT new br.dev.sam.customer.dto.CustomerDto(c.id, c.name, c.cpf, c.birthDate) " +
            " FROM Customer c " +
            " WHERE (:#{#filter.name} IS NULL OR LOWER(c.name) LIKE %:#{#filter.name}%) " +
            "   AND (:#{#filter.cpf}  IS NULL OR c.cpf  LIKE %:#{#filter.cpf}%) ")
    Page<CustomerDto> findCustomersByFilter(CustomerFilter filter, Pageable pageRequest);
}
