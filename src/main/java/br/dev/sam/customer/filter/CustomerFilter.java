package br.dev.sam.customer.filter;

public class CustomerFilter extends AbstractFilter {

    private String name;
    private String cpf;

    public CustomerFilter(String name, String cpf, int page, int size) {
        this.name = name;
        this.cpf = cpf;
        this.setupPage(page, size);
    }

    public String getName() {
        return name != null ? name.toLowerCase() : null;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}
