package br.dev.sam.customer.filter;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public abstract class AbstractFilter {

	private int page = 0;
	private int size = 10;

	public void setupPage(int page, int size) {
		this.page = page;
		this.size = size;
	}

	public Pageable createPageRequest() {
		return PageRequest.of(page, size);
	}
}
