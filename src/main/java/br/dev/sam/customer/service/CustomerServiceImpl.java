package br.dev.sam.customer.service;

import br.dev.sam.customer.dto.CustomerDto;
import br.dev.sam.customer.dto.CustomerPatchDto;
import br.dev.sam.customer.exception.CustomerNotFoundException;
import br.dev.sam.customer.filter.CustomerFilter;
import br.dev.sam.customer.interfaces.CustomerRequireds;
import br.dev.sam.customer.entity.Customer;
import br.dev.sam.customer.repository.CustomerRepository;
import br.dev.sam.customer.util.CpfUtils;
import br.dev.sam.customer.util.CustomerBuildUtils;
import br.dev.sam.customer.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository repository;

    @Override
    public CustomerDto insert(CustomerDto customerDto) {
        validateCustomer(customerDto);
        Customer customer = CustomerBuildUtils.buildCustomerEntity(customerDto);
        return CustomerBuildUtils.buildCustomerDto(repository.save(customer));
    }

    @Override
    public CustomerDto update(UUID id, CustomerDto customerDto) {
        validateCustomer(customerDto);
        Customer customer = CustomerBuildUtils.buildCustomerEntity(customerDto);
        customer.setId(id);
        return CustomerBuildUtils.buildCustomerDto(repository.save(customer));
    }

    @Override
    public CustomerDto partialUpdate(UUID id, CustomerPatchDto patch) {
        Customer customer = repository.findById(id).orElseThrow(CustomerNotFoundException::new);
        CustomerBuildUtils.applyPatchCustomer(patch, customer);
        validateCustomer(customer);
        return CustomerBuildUtils.buildCustomerDto(repository.save(customer));
    }

    @Override
    public void remove(UUID id) {
        Validator.checkArgument(repository.existsById(id), new CustomerNotFoundException());
        repository.deleteById(id);
    }

    @Override
    public CustomerDto findById(UUID id) {
        Customer customer = repository.findById(id).orElseThrow(CustomerNotFoundException::new);
        return CustomerBuildUtils.buildCustomerDto(customer);
    }

    @Override
    public Page<CustomerDto> find(String name, String cpf, Integer page, Integer size) {
        CustomerFilter filter = new CustomerFilter(name, cpf, page, size);
        return repository.findCustomersByFilter(filter, filter.createPageRequest());
    }

    private void validateCustomer(CustomerRequireds customer) {
        Validator.newInstance()
                .checkArgument(customer.getName() != null, "Campo obrigatório não informado.", "name")
                .checkArgument(customer.getCpf() != null, "Campo obrigatório não informado.", "cpf")
                .checkArgument(CpfUtils.isValid(customer.getCpf()), "O CPF informado não é válido.", "cpf")
                .throwExceptions();
    }
}
