package br.dev.sam.customer.service;

import br.dev.sam.customer.dto.CustomerDto;
import br.dev.sam.customer.dto.CustomerPatchDto;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface CustomerService {

    CustomerDto insert(CustomerDto customerDto);
    CustomerDto update(UUID id, CustomerDto customerDto);
    CustomerDto partialUpdate(UUID id, CustomerPatchDto patch);
    void remove(UUID id);
    CustomerDto findById(UUID id);
    Page<CustomerDto> find(String name, String cpf, Integer page, Integer size);
}
