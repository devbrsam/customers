package br.dev.sam.customer.dto;

import org.openapitools.jackson.nullable.JsonNullable;

import java.time.LocalDate;

public class CustomerPatchDto {

    private JsonNullable<String> name = JsonNullable.undefined();
    private JsonNullable<String> cpf = JsonNullable.undefined();
    private JsonNullable<LocalDate> birthDate = JsonNullable.undefined();

    public JsonNullable<String> getName() {
        return name;
    }

    public void setName(JsonNullable<String> name) {
        this.name = name;
    }

    public JsonNullable<String> getCpf() {
        return cpf;
    }

    public void setCpf(JsonNullable<String> cpf) {
        this.cpf = cpf;
    }

    public JsonNullable<LocalDate> getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(JsonNullable<LocalDate> birthDate) {
        this.birthDate = birthDate;
    }
}
