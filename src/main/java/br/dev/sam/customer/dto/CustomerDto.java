package br.dev.sam.customer.dto;

import br.dev.sam.customer.interfaces.CustomerRequireds;
import br.dev.sam.customer.util.DateUtils;

import java.time.LocalDate;
import java.util.UUID;

public class CustomerDto implements CustomerRequireds {

    private UUID id;
    private String name;
    private String cpf;
    private LocalDate birthDate;
    private Long age;

    public CustomerDto(UUID id, String name, String cpf, LocalDate birthDate) {
        this.id = id;
        this.name = name;
        this.cpf = cpf;
        this.birthDate = birthDate;
        this.age = birthDate != null ? DateUtils.yearsBetween(birthDate, LocalDate.now()) : null;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public static final class Builder {

        private UUID id;
        private String name;
        private String cpf;
        private LocalDate birthDate;

        private Builder() {}

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder id(UUID id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder cpf(String cpf) {
            this.cpf = cpf;
            return this;
        }

        public Builder birthDate(LocalDate birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public CustomerDto build() {
            return new CustomerDto(id, name, cpf, birthDate);
        }
    }
}
