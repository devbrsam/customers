package br.dev.sam.customer.interfaces;

public interface CustomerRequireds {

    String getName();
    String getCpf();
}
