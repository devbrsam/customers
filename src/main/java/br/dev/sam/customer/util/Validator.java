package br.dev.sam.customer.util;

import br.dev.sam.customer.exception.ErrorDetail;
import br.dev.sam.customer.exception.ValidationException;

import java.util.ArrayList;
import java.util.List;

public final class Validator {

    private List<ErrorDetail> errors = new ArrayList<>();

    private Validator() {}

    public static Validator newInstance() {
        return new Validator();
    }

    public Validator checkArgument(boolean condition, String message, String field) {
        if (!condition) {
            ErrorDetail error = new ErrorDetail(message, field);
            errors.add(error);
        }
        return this;
    }

    public static void checkArgument(boolean condition, RuntimeException exception) {
        if (!condition) {
            throw exception;
        }
    }

    public void throwExceptions() {
        if (!errors.isEmpty()) {
            throw new ValidationException(errors);
        }
    }
}
