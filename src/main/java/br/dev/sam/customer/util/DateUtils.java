package br.dev.sam.customer.util;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class DateUtils {

    private DateUtils() {}

    public static Long yearsBetween(LocalDate startDate, LocalDate endDate) {
        return ChronoUnit.YEARS.between(startDate, endDate);
    }
}
