package br.dev.sam.customer.util;

public class CpfUtils {

    private static final int[] factor = {11, 10, 9, 8, 7, 6, 5, 4, 3, 2};

    private CpfUtils() {}

    public static boolean isValid(String cpf) {
        if ((cpf == null) || (cpf.length() != 11)) {
            return false;
        }
        try {
            Integer digit1 = calculateDigit(cpf.substring(0, 9), factor);
            Integer digit2 = calculateDigit(cpf.substring(0, 9) + digit1, factor);
            return cpf.equals(cpf.substring(0, 9) + digit1.toString() + digit2.toString());
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    private static int calculateDigit(String str, int[] factor) {
        int sum = 0;
        for (int indice = str.length() - 1; indice >= 0; indice--) {
            int digito = Integer.parseInt(str.substring(indice, indice + 1));
            sum += digito * factor[factor.length - str.length() + indice];
        }
        sum = 11 - sum % 11;
        return sum > 9 ? 0 : sum;
    }
}