package br.dev.sam.customer.util;

import br.dev.sam.customer.dto.CustomerDto;
import br.dev.sam.customer.dto.CustomerPatchDto;
import br.dev.sam.customer.entity.Customer;

public class CustomerBuildUtils {

    private CustomerBuildUtils() {}

    public static Customer buildCustomerEntity(CustomerDto customer) {
        return Customer.Builder.newInstance()
                .name(customer.getName())
                .cpf(customer.getCpf())
                .birthDate(customer.getBirthDate())
                .build();
    }

    public static CustomerDto buildCustomerDto(Customer customer) {
        return CustomerDto.Builder.newInstance()
                .id(customer.getId())
                .name(customer.getName())
                .cpf(customer.getCpf())
                .birthDate(customer.getBirthDate())
                .build();
    }

    public static void applyPatchCustomer(CustomerPatchDto patch, Customer customer) {
        JsonNullableUtils.changeIfPresent(patch.getName(), customer::setName);
        JsonNullableUtils.changeIfPresent(patch.getCpf(), customer::setCpf);
        JsonNullableUtils.changeIfPresent(patch.getBirthDate(), customer::setBirthDate);
    }
}
