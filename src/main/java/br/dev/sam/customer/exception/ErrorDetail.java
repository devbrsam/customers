package br.dev.sam.customer.exception;

import java.io.Serializable;

public class ErrorDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    private String message;
    private String field;

    public ErrorDetail(String message, String field) {
        super();
        this.message = message;
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public String getField() {
        return field;
    }
}