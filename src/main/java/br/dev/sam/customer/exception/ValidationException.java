package br.dev.sam.customer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class ValidationException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	private final List<ErrorDetail> errors;
	
	public ValidationException(List<ErrorDetail> errors) {
		this.errors = errors;
	}

	public List<ErrorDetail> getErrors() {
		return errors;
	}
}
