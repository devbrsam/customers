package br.dev.sam.customer.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(CustomerNotFoundException.class)
    public ResponseEntity<String> handleCustomerNotFoundException(CustomerNotFoundException ex) {
        return ResponseEntity.status(CustomerNotFoundException.class.getAnnotation(ResponseStatus.class).code()).body(ex.getMessage());
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<List<ErrorDetail>> handleCustomerValidationException(ValidationException ex) {
        return ResponseEntity.status(ValidationException.class.getAnnotation(ResponseStatus.class).code()).body(ex.getErrors());
    }
}
