package br.dev.sam.customer.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

    @Bean
    public OpenAPI api() {
        return new OpenAPI().info(apiData());
    }

    private Info apiData() {
        Contact contact = new Contact();
        contact.setName("Samuel Mendes Vieira");
        contact.setEmail("devbrsam@gmail.com");

        return new Info()
                .title("API Clientes")
                .description("API para gerenciamento de clientes.")
                .contact(contact)
                .version("1.0.0");
    }
}
