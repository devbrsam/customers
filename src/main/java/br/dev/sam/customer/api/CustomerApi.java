package br.dev.sam.customer.api;

import br.dev.sam.customer.dto.CustomerDto;
import br.dev.sam.customer.dto.CustomerPatchDto;
import br.dev.sam.customer.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

@Tag(name = "Customers", description = "API dedicada ao gerenciamento de clientes.")
@RestController
@RequestMapping(path = "/customers")
public class CustomerApi {

    @Autowired
    private CustomerService service;

    @Operation(summary = "Realiza a inserção de um cliente na base de dados.")
        @ApiResponse(responseCode = "201", description = "Cliente inserido com sucesso.")
        @ApiResponse(responseCode = "400", description = "Erro na validação dos dados enviados.")
        @ApiResponse(responseCode = "500", description = "Erro interno no servidor.")
    @PostMapping
    public ResponseEntity<CustomerDto> insert(@RequestBody CustomerDto customer) {
        customer = service.insert(customer);
        URI uri = setupUri(customer.getId());
        HttpHeaders headers = setupHeader(uri);
        return new ResponseEntity<>(customer, headers, HttpStatus.CREATED);
    }

    @Operation(summary = "Realiza a alteração de um cliente na base de dados.")
        @ApiResponse(responseCode = "200", description = "Cliente alterado com sucesso.")
        @ApiResponse(responseCode = "400", description = "Erro na validação dos dados enviados.")
        @ApiResponse(responseCode = "500", description = "Erro interno no servidor.")
    @PutMapping("/{id}")
    public ResponseEntity<CustomerDto> update(@PathVariable("id") UUID id, @RequestBody CustomerDto customer) {
        return ResponseEntity.ok(service.update(id, customer));
    }

    @Operation(summary = "Realiza a alteração de campos específicos de um cliente na base de dados.",
                description = "- Informe apenas os campos que devem ser alterados. \n " +
                              "- Para alterar o valor de um campo para 'null', defina o campo como null (sem aspas).")
        @ApiResponse(responseCode = "200", description = "Cliente alterado com sucesso.")
        @ApiResponse(responseCode = "400", description = "Erro na validação dos dados enviados.")
        @ApiResponse(responseCode = "500", description = "Erro interno no servidor.")
    @PatchMapping("/{id}")
    public ResponseEntity<CustomerDto> partialUpdate(@PathVariable UUID id, @RequestBody CustomerPatchDto patch) {
        return ResponseEntity.ok(service.partialUpdate(id, patch));
    }

    @Operation(summary = "Realiza a exclusão de um cliente na base de dados.")
        @ApiResponse(responseCode = "200", description = "Cliente removido com sucesso.")
        @ApiResponse(responseCode = "404", description = "Cliente não encontrado.")
        @ApiResponse(responseCode = "500", description = "Erro interno no servidor.")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> remove(@PathVariable("id") UUID id) {
        service.remove(id);
        return ResponseEntity.ok("Cliente removido com sucesso: " + id);
    }

    @Operation(summary = "Recupera as informações de um cliente na base de dados, de acordo com o ID informado.")
        @ApiResponse(responseCode = "200", description = "Cliente encontrado.")
        @ApiResponse(responseCode = "404", description = "Cliente não encontrado.")
        @ApiResponse(responseCode = "500", description = "Erro interno no servidor.")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @GetMapping("/{id}")
    public CustomerDto findById(@PathVariable("id") UUID id) {
        return service.findById(id);
    }

    @Operation(summary = "Lista os cliente existentes na base de dados, de acordo com o nome e/ou CPF informados.",
            description = "- Caso não sejam informados um nome ou CPF, todos os clientes serão listados. \n " +
                          "- Por default, são retornados os primeiros 10 registros da base. Para alterar a paginação, preencha os campos 'page' e 'size'.")
        @ApiResponse(responseCode = "200", description = "Clientes encontrados.")
        @ApiResponse(responseCode = "500", description = "Erro interno no servidor.")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @GetMapping
    public Page<CustomerDto> find(@RequestParam(value = "name", required = false) String name,
                                  @RequestParam(value = "cpf", required = false) String cpf,
                                  @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                  @RequestParam(value = "size", required = false, defaultValue = "10") Integer size) {
        return service.find(name, cpf, page, size);
    }

    private URI setupUri(UUID id) {
        return ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(id).toUri();
    }

    private HttpHeaders setupHeader(URI uri) {
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uri);
        return headers;
    }
}
