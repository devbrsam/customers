package br.dev.sam.customer.util;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class DateUtilsTest {

    @Test
    public void yearsBetween_mustReturnNumberOfYearBetweenDates() {
        LocalDate startDate = LocalDate.of(1999,10,1);
        LocalDate endDate = LocalDate.of(2020,10,31);

        Long result = DateUtils.yearsBetween(startDate, endDate);

        assertEquals(Long.valueOf(21), result);
    }
}
