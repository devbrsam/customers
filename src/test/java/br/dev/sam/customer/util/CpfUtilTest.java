package br.dev.sam.customer.util;

import org.junit.Assert;
import org.junit.Test;

public class CpfUtilTest {

	@Test
	public void isValid_mustReturnTrueWhenCpfIsValid() {
		String cpf = "26472762007";
		
		Boolean isValid = CpfUtils.isValid(cpf);
		
		Assert.assertTrue(isValid);
	}
	
	@Test
	public void isValid_mustReturnFalseWhenCpfIsNotValid() {
		testValidCpf("26472762011");
	}
	
	@Test
	public void isValid_mustReturnFalseWhenCpfIsNotCompleted() {
		testValidCpf("222");
	}
	
	@Test
	public void isValid_mustReturnFalseWhenCpfContainsSimbols() {
		testValidCpf("26472762?00");
	}

	@Test
	public void isValid_mustReturnFalseWhenCpfContainsMathSimbols() {
		testValidCpf("2647276200+");
	}
	
	@Test
	public void isValid_mustReturnFalseWhenCpfIsNull() {
		testValidCpf(null);
	}

	private void testValidCpf(String o) {
		String cpf = o;

		Boolean isValid = CpfUtils.isValid(cpf);

		Assert.assertFalse(isValid);
	}
}
