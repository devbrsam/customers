FROM maven:3.6.3-jdk-11-slim as target
WORKDIR /build
COPY pom.xml .

COPY src/ /build/src/
RUN mvn package

FROM openjdk:11-jre-slim-buster
EXPOSE 8000
CMD exec java -Duser.timezone=America/Sao_Paulo -jar /app/customer-1.0.0.jar
COPY --from=target /build/target/customer-1.0.0.jar /app/customer-1.0.0.jar